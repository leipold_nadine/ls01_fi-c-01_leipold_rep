import java.util.Scanner;

public class Aufgabe_2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		double nettowert = nettowerteingabe();
		boolean steuersatz = steuersatzeingabe();
		berechnung(nettowert, steuersatz);
		
	}
	
	public static double nettowerteingabe() {
		System.out.println("Nettowert eingeben:");
		Scanner tastatur = new Scanner(System.in);
		double nettowert = tastatur.nextDouble();
		return nettowert;
		
	}
	
	public static boolean steuersatzeingabe() {
		System.out.println("Steuersatz?\n j f�r Ja, n f�r nein.");
		Scanner tastatur = new Scanner(System.in);
		String steuersatz = tastatur.next();
		if(steuersatz.equals("j")){
			  return true;
		}
		else {
			return false;
		}
	}
	
	public static void berechnung(double nettowert, boolean steuersatz) {
		if (steuersatz) {
			double bruttowert = (nettowert / 100 * 19)+(nettowert);
			System.out.println("Bruttowert:" + bruttowert);
		}
		else {
			System.out.println("Ohne Steuern angegeben.");
		}
	}

}
