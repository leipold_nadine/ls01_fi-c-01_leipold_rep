import java.util.Scanner;
 
public class switch_case {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner newScanner = new Scanner (System.in);
		
		System.out.println("Geben sie ihre Note ein");
		int note = newScanner.nextInt();
		
		switch (note) {
		case 1:
			System.out.println("Sehr gut");
			break;
		case 2: 
			System.out.println("Gut");
			break;
		case 3:
			System.out.println("Befriedigend");
			break;
		case 4:
			System.out.println("Ausreichend");
			break;
		case 5:
			System.out.println("Mangelhaft");
		case 6:
			System.out.println("Ungenügend");
			break;
		default:
			System.out.println("Bitte eine Zahl von 1-6");
			break;
		}
	}

}
