import java.util.Scanner;
 
public class Aufgabe_1 {

	public static void main(String[] args) {
		
		int Zahl = eingabe();
		ausgabe(Zahl);

	}
	
	public static int eingabe() {
		System.out.println("Bitte geben Sie eine Zahl von 1-6 ein.");
		Scanner tastatur = new Scanner(System.in);
		int Zahl = tastatur.nextInt();
		return Zahl;
	}
	
	
	public static void ausgabe(int Zahl) {
		if (Zahl ==  1) {
			System.out.println("Sehr gut");
		}
		else if (Zahl == 2) {
			System.out.println("Gut");
		}
		else if (Zahl == 3) {
			System.out.println("Befriedigend");
		}
		else if (Zahl == 4) {
			System.out.println("Ausreichend");
		}
		else if (Zahl == 5) {
			System.out.println("Mangelhaft");
		}
		else if (Zahl == 6) {
			System.out.println("Ungenügend");
		}
		else {
			System.out.println("Fehler, bitte nur eine Zahl von 1-6");
		}
	}
	
}
