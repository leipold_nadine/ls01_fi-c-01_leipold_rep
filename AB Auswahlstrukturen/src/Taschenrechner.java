import java.util.Scanner;

public class Taschenrechner {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("Bitte erste Zahl angeben:");
		Scanner tastatur = new Scanner (System.in);
		int zahl1 = tastatur.nextInt();
		System.out.println("Zweite Zahl angeben:");
		Scanner tastatur2 = new Scanner (System.in);
		int zahl2 = tastatur.nextInt();
		
		ausgabe(zahl1, zahl2);

	}
	
	public static void ausgabe(int zahl1, int zahl2) {
		System.out.println("Sollen die Zahlen addiert( + ), subtrahiert( - ), multipliziert( * ) oder dividiert( : ) werden? Bitte das Symbol angeben.");
		Scanner tastatur = new Scanner (System.in);
		char Symbol = tastatur.next().charAt(0);
		
		if (Symbol == '+') {
			int rechnung = zahl1 + zahl2;
			System.out.println("Ergebnis:"+ rechnung);
		}
		else if (Symbol == '-') {
			int rechnung = zahl1 - zahl2;
			System.out.println("Ergebnis:"+ rechnung);
		}
		else if (Symbol == '*') {
			int rechnung = zahl1 * zahl2;
			System.out.println("Ergebnis:"+ rechnung);
		}
		else if (Symbol == ':') {
			int rechnung = zahl1 / zahl2;
			System.out.println("Ergebnis:"+ rechnung);
		}
		else {
			System.out.println("Bitte ein Symbol angeben");
		}
			
	}
	


}
