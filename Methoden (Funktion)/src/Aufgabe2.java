import java.util.Scanner;

public class Aufgabe2 {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		 

		// Benutzereingaben lesen
		System.out.println("was m�chten Sie bestellen?");
		String artikel = liesString(myScanner.next());
		
		System.out.println("Geben Sie die Anzahl ein:");
		int anzahl = liesInt(myScanner.next());
		
		System.out.println("Geben Sie den Nettopreis ein:");
		double preis = liesDouble(myScanner.next());

		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		double mwst = liesDouble(myScanner.next());

		// Verarbeiten
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);

		// Ausgeben

		rechnungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);
	}
	
	// Funktionen
	//EIngabe
	public static String liesString(String text)
	{
		return text;
	}
	
	public static int liesInt(String text)
	{
		int zahl = Integer.parseInt(text);
		return zahl;
	}
	public static double liesDouble(String text)
	{
		double zahl = Double.parseDouble(text);
		return zahl;
	}
	
	//Verarbeitung
	public static double berechneGesamtnettopreis(int anzahl, double nettopreis)
	{
		double zahl = (anzahl * nettopreis);
		return zahl;
	}
	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst)
	{
		double zahl = (nettogesamtpreis * (1 + mwst / 100));
		return zahl;
	}
	
	//Ausgabe
	public static void rechnungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis,double mwst)
	{
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}
}