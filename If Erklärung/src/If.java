
public class If {
	
	public static void main (String[] args) {
		
		//Zuweisung
		//int = zahl3, zahl4;
		
		int zahl1 = 23; //deklaration und Initialisierung
		int zahl2 = 75;
		
		//if Schl�sselwort zur Auswahl
		
		//Bedingung: wenn Zahl 1 kleiner ist als Zahl 2
		if (zahl1 < zahl2) {
			System.out.println("Zahl 1 ist kleiner als Zahl 2");
			
		}
		else if (zahl1 > zahl2) {
			System.out.println("Zahl 1 ist gr��er als Zahl 2");
			// else = wenn die bedingung nicht stimmt gib das aus
			// else if = wenn das erste nicht stimmt, pr�fe das hier
		}
		else {
			System.out.println("Zahl 1 ist gleich Zahl 2");
		}
	}
}
