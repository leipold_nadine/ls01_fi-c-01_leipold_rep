﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
    	while(true) {
    		double zuZahlen = FahrkartenbestellungErfassen();
        	double einzahlung = FahrkartenBezahlen(zuZahlen);
        	FahrkartenAusgabe();
        	RückgeldAusgabe(zuZahlen, einzahlung);
    	}
    }
    
    public static double FahrkartenbestellungErfassen() {
    	Scanner tastatur = new Scanner (System.in);
    	System.out.println("Wählen Sie ihre Wunschfahrkarte aus:\n");
    	
    	String[ ] fahrkartenbezeichnung = {"Einzelfahrschein Berlin AB","Einzelfahrschein Berlin BC" ,"Einzelfahrschein Berlin ABC","Kurzstrecke","Tageskarte Berlin AB","Tageskarte Berlin BC","Tageskarte Berlin ABC","Kleingruppen-Tageskarte Berlin AB","Kleingruppen-Tageskarte Berlin BC","Kleingruppen-Tageskarte Berlin ABC"};
    	double[ ] fahrkartenpreise = {2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};
    	
    	for (int i = 0; i < fahrkartenbezeichnung.length; i++) {
			System.out.printf(fahrkartenbezeichnung[i]+ ", Preis: " + "%.2f €\n" , fahrkartenpreise[i]);
		}
    	
    	int fahrkartentyp = 0;
    	double zuZahlenderBetrag = 0;
        
    	
    	 do {	
    		fahrkartentyp = tastatur.nextInt();
    		if (fahrkartentyp <= 10 ) {
    			zuZahlenderBetrag = fahrkartenpreise[fahrkartentyp -1];
    			System.out.printf("Ihre Wahl: " + fahrkartenbezeichnung[fahrkartentyp -1] + "\n" + "Ticketpreis: %.2f€\n" , zuZahlenderBetrag);
    			}
    		else {
    			System.out.println(">>falsche Eingabe<<\n");
    		}
		} while (fahrkartentyp > 10);
		

    	
    	System.out.println("Wie viele Fahrkarten? (Max. 1 - 10)");
		byte anzahl = tastatur.nextByte();
		//System.out.println("Zu zahlender Betrag");
		//double zuZahlenderBetrag = tastatur.nextDouble();
		
		if (anzahl < 1 || anzahl > 10 && zuZahlenderBetrag < 0) {
			System.out.println("Ungültiger Wert eingegeben! Mit Anzahl 1 und Preis 1€ fortfahren.");
			anzahl = 1;
			zuZahlenderBetrag = 1;
			return anzahl*zuZahlenderBetrag;
		}
		else if (zuZahlenderBetrag < 0){
			System.out.println("Ungültiger Wert eingegeben! Mit Preis von 1€ fortfahren.");
			zuZahlenderBetrag = 1;
			return anzahl*zuZahlenderBetrag;
		}
		else if (anzahl < 1 || anzahl > 10  ) {
			System.out.println("Ungültiger Wert eingegeben! Mit Anzahl 1 fortfahren.");
			anzahl = 1;
			return anzahl*zuZahlenderBetrag;
		}
		else {
			return anzahl*zuZahlenderBetrag;
		}
		
	}
    
    public static double FahrkartenBezahlen(double zuZahlen) {
    	// Geldeinwurf
        // -----------
    	Scanner tastatur = new Scanner(System.in);
        double eingezahlterGesamtbetrag = 0.00;
        double eingeworfeneMünzen = 0;
        while(eingezahlterGesamtbetrag < zuZahlen)
        {
     	   System.out.printf("Noch zu zahlen: %.2f Euro ", (zuZahlen - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünzen = tastatur.nextDouble();
     	   eingezahlterGesamtbetrag += eingeworfeneMünzen;
        }
        return eingezahlterGesamtbetrag;
    }
	   
    public static void FahrkartenAusgabe() {
    	//Fahrscheinausgabe
    	// -----------------
    	System.out.println("\nFahrschein wird ausgegeben");
    	for (int i = 0; i < 8; i++)
    	{
    		System.out.print("=");
    		try {
    			Thread.sleep(250);
    		} catch (InterruptedException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    	}
    	System.out.println("\n\n");
    }

    public static void RückgeldAusgabe(double zuZahlen, double eingeworfeneMünzen ) {
       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       double rückgabebetrag = eingeworfeneMünzen - zuZahlen + 0.00000001;
       if(rückgabebetrag > 0.0)
       {
    	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f Euro ", rückgabebetrag);
    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

           while(rückgabebetrag >= 2.00) // 2 EURO-Münzen
           {
        	  System.out.println("2 EURO");
	          rückgabebetrag -= 2.00;
           }
           while(rückgabebetrag >= 1.00) // 1 EURO-Münzen
           {
        	  System.out.println("1 EURO");
	          rückgabebetrag -= 1.00;
           }
           while(rückgabebetrag >= 0.50) // 50 CENT-Münzen
           {
        	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.50;
           }
           while(rückgabebetrag >= 0.20) // 20 CENT-Münzen
           {
        	  System.out.println("20 CENT");
 	          rückgabebetrag -= 0.20;
           }
           while(rückgabebetrag >= 0.10) // 10 CENT-Münzen
           {
        	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.10;
           }
           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
           {
        	  System.out.println("5 CENT");
 	          rückgabebetrag -= 0.05;
           }
           while(rückgabebetrag == 0);
           {
        	   System.out.println("Ende");
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.\n");
    }
}